import React,{useState,useEffect,useRef} from 'react';
import axios from 'axios';
import CloudIcon from '@material-ui/icons/Cloud';
import Brightness5Icon from '@material-ui/icons/Brightness5';


const api={
  key:"00d6f78817013d8d386287537144d279",
  base:"https://api.openweathermap.org/data/2.5/"
}

function App() {

 const [initial,setinitial]=useState([])
useEffect(()=>{
  async function fetchdata(){
    const response=await  axios(`https://api.openweathermap.org/data/2.5/weather?q=tehran&units=metric&APPID=00d6f78817013d8d386287537144d279`);
    setinitial(response.data);
    return response

  }
  fetchdata()
  console.log('title',initial);
},[])
 
  
  
      const [query, setQuery] = useState('');
    

  const search = evt => {
    if (evt.key === "Enter") {
      axios(`${api.base}weather?q=${query}&units=metric&APPID=${api.key}`)
    .then(result => {
          setinitial(result.data);
          setQuery('');
          console.log(result);
        });
    }
  }
  const dateBuilder = (d) => {
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let day = days[d.getDay()];
    let date = d.getDate();
    let month = months[d.getMonth()];
    let year = d.getFullYear();

    return `${day}, ${date} ${month} ${year}`
  }
  const [timer,setTimer]=useState("")
  useEffect(()=>{
   const d=new Date()
   const hours=d.getHours()
   const min=d.getMinutes()
   const seconds=d.getSeconds()
  const interval= setInterval(()=>{
   setTimer(`${hours}:${min}:${seconds}`)
   },1000)
   return()=>{
      clearInterval(interval)}
  },[timer])
  
  return (
  <>
 
  <div className={(typeof initial.main != "undefined") ? ((initial.main.temp > 16) ? 'app warm' : 'app') : 'app'}>

     
           {typeof initial.main!="undefined"?
           <>
           <div className="general">
          
           <div className="temp-d">
          <div className="f">
           <p>{timer}</p>
           <span className="date">{dateBuilder(new Date())}</span>
           </div>
      
        
       
          <p style={{fontSize:"10rem"}}> {Math.round(initial.main.temp)}°c</p>
          </div>

           
           <div className="temp">
           {initial.weather[0].main==="Clouds"?<CloudIcon vatiant="primery" style={{width:"150px",height:"150px"}}/>:<Brightness5Icon style={{width:"150px",height:"150px"}}/>}
        
         <input
          error
          id="outlined-error"
          label="Search..." 
          defaultValue="Tehran"
          variant="outlined"
          className="search-bar"
      
            onChange={e => setQuery(e.target.value)}
            value={query}
            onKeyPress={search}
            autoComplete="off"
        />
      
            <span style={{marginRight:"46px"}}>{initial.name}, {initial.sys.country}</span>
            <div class="hline"></div>
            <div className="location"><div>temp</div>   <div>{Math.round(initial.main.temp)}°c</div> </div>
            <div class="hline"></div>
            <div className="location"><span >humidity</span> <span>{initial.main.humidity}</span>   </div>
            <div class="hline"></div>
            <div className="location"><span >tempMax</span>   <span>{initial.main.temp_max}°c</span> </div>
            <div class="hline"></div>
            <div className="location"><span >tempMin</span>  <span>{initial.main.temp_min}°c</span>  </div>
            <div class="hline"></div>
            <div className="date">{dateBuilder(new Date())}</div>
           
          
            </div>
            
            </div>
            


          
              </>
            :""}  
    
    </div>
  
  </>
  );
}

export default App;
/*
  
*/